import express from "express";
import cors from "cors";
import mainRouter from "./router";
import { APP_BASE_URL } from "./config";
console.log(APP_BASE_URL);

export const createServer = () => {
  //initialisation du server
  const server: express.Application = express();

  server.use(express.json());

  // accès au http://localhost:1234
  server.use(
    cors({
      origin: "http://localhost:1234",
    })
  );

  //Ajout de la route au server
  server.use(APP_BASE_URL, mainRouter);
  // server.use("/", mainRouter);
  return server;
};
