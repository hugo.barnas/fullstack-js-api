import { createServer } from "./server";
import { PORT, NODE_ENV } from "./config";

const main = async () => {
  const server = await createServer();
  server.listen(process.env.PORT, () => {
    console.log(`Server is now running on port ${PORT} in ${NODE_ENV} mode`);
  });
};
main();
