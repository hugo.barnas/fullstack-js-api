import { Router, Request, Response } from "express";

const mainRouter: Router = Router();
//création d'une route get nommée mainRouter qui sert de test
// mainRouter.get("/", (_: Request, res: Response) => {
//   res.json("Phrase de test #1");
// });
mainRouter.get("/", (_: Request, res: Response) => {
  // "_" car Request n'est jamais utilisé
  res.send("Racine de l'API");
});

export default mainRouter;
