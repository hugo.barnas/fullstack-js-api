import dotenv from "dotenv";
import path from "path";

//on spécifie le chemin du .env grace au path
const envPath = path.join(__dirname, "../../");
dotenv.config({
  path: envPath + "./.env",
});

//exportation des constantes PORT et API_BASE_URL
export const NODE_ENV = process.env.NODE_ENV || "development";
export const PORT = process.env.PORT || "3305"; //export du process.env et si il est vide il faut prendre le port 3305
export const APP_BASE_URL = process.env.APP_BASE_URL || "/api/v1/"; //utiliser le APP_BASE_URL dont la valeur est spécifiée dans le .env et si la valeur n'existe pas prendre à la place le chemin app/v1 -> il est important de spécifier l'un ou l'autre car TypeScript fera un commentaire si l'éventualité n'est pas spécifiée
