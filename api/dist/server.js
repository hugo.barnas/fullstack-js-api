"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const router_1 = __importDefault(require("./router"));
const config_1 = require("./config");
console.log(config_1.APP_BASE_URL);
const createServer = () => {
    //initialisation du server
    const server = express_1.default();
    server.use(express_1.default.json());
    // accès au http://localhost:1234
    server.use(cors_1.default({
        origin: "http://localhost:1234",
    }));
    //Ajout de la route au server
    server.use(config_1.APP_BASE_URL, router_1.default);
    // server.use("/", mainRouter);
    return server;
};
exports.createServer = createServer;
//# sourceMappingURL=server.js.map