"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mainRouter = express_1.Router();
//création d'une route get nommée mainRouter qui sert de test
// mainRouter.get("/", (_: Request, res: Response) => {
//   res.json("Phrase de test #1");
// });
mainRouter.get("/", (_, res) => {
    res.send("Phrase de test #2");
});
exports.default = mainRouter;
//# sourceMappingURL=routeur.js.map