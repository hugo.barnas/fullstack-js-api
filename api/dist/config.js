"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.APP_BASE_URL = exports.PORT = exports.NODE_ENV = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
//on spécifie le chemin du .env grace au path
const envPath = path_1.default.join(__dirname, "../../");
dotenv_1.default.config({
    path: envPath + "./.env",
});
//exportation des constantes PORT et API_BASE_URL
exports.NODE_ENV = process.env.NODE_ENV || "development";
exports.PORT = process.env.PORT || "3305"; //export du process.env et si il est vide il faut prendre le port 3305
exports.APP_BASE_URL = process.env.APP_BASE_URL || "/api/v1/"; //utiliser le APP_BASE_URL dont la valeur est spécifiée dans le .env et si la valeur n'existe pas prendre à la place le chemin app/v1 -> il est important de spécifier l'un ou l'autre car TypeScript fera un commentaire si l'éventualité n'est pas spécifiée
//# sourceMappingURL=config.js.map