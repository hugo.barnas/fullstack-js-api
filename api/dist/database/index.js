"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.prisma = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
const client_1 = require("@prisma/client");
//Nous avons besoin ici de remettre cette config avec dotenv
//Pour que Prisma puisse aller chercher où se trouve le fichier .env qui contiendra les informations de notre BDD
const envPath = path_1.default.join(__dirname, "../../");
dotenv_1.default.config({ path: envPath + ".env" });
const prisma = new client_1.PrismaClient({ log: ["query"] });
exports.prisma = prisma;
//# sourceMappingURL=index.js.map