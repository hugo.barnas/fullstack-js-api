//export default on renome instance par api
import api from "./utils/api";

const greetings = async () => {
  // const message: string = "HELLO";
  // console.log(typeof message);
  // console.log(message);
  //Création d'un get
  const axios = await api.get("/");
  console.log("Axios response ->", axios);
  console.log("Axios response data -> ", axios.data);
  //création d'un get plus particulier exemple pour les livres
  //console.log(await api.get("/books"))
  //Creation d'un put pour modifier un livre
  //console.log(await api.post("/books/1234"))
};

greetings();

const toggleThemeButton = document.getElementById("toggle-switch-theme-color");
toggleThemeButton.addEventListener("click", () => {
  document.body.classList.toggle("dark");
});
console.log(toggleThemeButton);
