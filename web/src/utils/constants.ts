import dotenv from "dotenv";
import path from "path";

const envPath = path.join(__dirname, "./../../../");
dotenv.config({ path: envPath + "./.env" });

export const API_BASE_URL =
  process.env.API_BASE_URL || "http://localhost:3305/api/v1"; //si par défaut il n'y a rien dans le .env alors on met ce chemain
