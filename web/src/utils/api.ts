//call d'api
//Création d'une instance d'axios qui sera utilisée partout dans notre application

import axios from "axios";
import { API_BASE_URL } from "./constants"; //importation de la constance API qui pourra par la suite passer de la v1 à la v2 car les bases de données peuvent être différentes

const instance = axios.create({
  baseURL: API_BASE_URL,
});

export default instance;
