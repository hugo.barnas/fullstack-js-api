# API REST and Front with REACT.js
---------------------------------
This is an exercice to know how build the backend of an web application with an api REST with the frontend with the template REACT.js

# How to run the project ?
---------------------------------

1. Run The commands : 
- `npm install` --> for each parts of the project : api and web
- install .env files to put the port and environnement of development and production for the server in your src source : 
example : 
PORT = 3305
NODE_DEVELOPMENT = development
NODE_PROD = production


2. for backend : 
- `npm run watch` --> for typescript to update in real time the dist folder
- `npm run dev` --> to start nodemon ( at ol that helps devleop node.js based applications by automatically restarting the node application when file changes in the directory are detected)
- `npm run buid` --> to build the folder

3. for frontend : 
- `npm run watch`

# DataBase
---------------------------------
1. Don't forget to put in your `.env` your src source

# Dependencies
---------------------------------



# Diagrams
---------------------------------

## Use Case Diagramm
---------------------------------

## Sequence Diagramm
---------------------------------

## Class Diagramm
---------------------------------

